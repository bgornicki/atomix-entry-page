import Vue from 'vue'
import DynamicImage from '~/client/components/elements/global/Dynamic.image'
import AtomixArrow from '~/client/components/elements/global/Atomix.arrow'

Vue.component('dimage', DynamicImage)
Vue.component('aArrow', AtomixArrow)
