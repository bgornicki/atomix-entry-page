// =================== DANGER ========================
// Use global mixin sparsely and carefully, because it affects
// every single Vue instance created, including third party components.
// =================== DANGER ========================

import Vue from 'vue'
import ENV_VARS from './env'

Vue.mixin({
  created: function() {
    process.ENV_VARS = ENV_VARS
  }
})
