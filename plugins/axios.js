// ================== DANGER =================
// = !!! THIS PLUGINS IS ONLY FOR CLIENT !!! =
// ====== SSR VERSION IS IN NUXT.CONFIG ======
// ================== DANGER =================

import axios from 'axios'

// Request interceptor
axios.interceptors.request.use(
  config => {
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// Response interceptor
axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    return Promise.reject(error)
  }
)
