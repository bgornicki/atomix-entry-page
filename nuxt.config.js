const pkg = require('./package')
module.exports = {
  mode: 'universal',

  dir: {
    assets: 'client/assets',
    components: 'client/components',
    layouts: 'layouts',
    middleware: 'middleware',
    pages: 'client/pages',
    static: 'static',
    store: 'store'
  },

  buildDir: 'public-vue',

  /*
   ** Headers of the page
   */
  head: {
    title: 'ATOMIX - BURN BUILD BREATHE',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=1.0'
      },
      { author: 'https://atomixburn.com/' },
      { version: pkg.version },
      {
        hid: 'description',
        name: 'description',
        content:
          'Your workout should fit your lifestyle. That is why at ATOMIX we offer a unique studio ambiance with top-class service and personal assistance / care / attendance.'
      },
      { property: 'og:locale', content: 'en_US' },
      { property: 'og:type', content: 'website' },
      {
        property: 'og:title',
        content: 'ATOMIX - BURN BUILD BREATHE'
      },
      {
        property: 'og:description',
        content:
          'Your workout should fit your lifestyle. That is why at ATOMIX we offer a unique studio ambiance with top-class service and personal assistance / care / attendance.'
      },
      { property: 'og:url', content: 'https://atomixburn.com/' },
      { property: 'og:site_name', content: 'ATOMIX - BURN BUILD BREATHE' },
      // { property: 'fb:app_id', content: 'xxxxxxxxxxxxx' },
      {
        property: 'og:image',
        content:
          'https://atomixburn.com/wp-content/uploads/2016/02/default_bg.jpg'
      },
      {
        property: 'og:image:secure_url',
        content:
          'https://atomixburn.com/wp-content/uploads/2016/02/default_bg.jpg'
      },
      { name: 'twitter:card', content: 'summary_large_image' },
      {
        name: 'twitter:description',
        content:
          'Your workout should fit your lifestyle. That is why at ATOMIX we offer a unique studio ambiance with top-class service and personal assistance / care / attendance.'
      },
      {
        name: 'twitter:title',
        content: 'ATOMIX - BURN BUILD BREATHE'
      },
      {
        name: 'twitter:image',
        content:
          'https://atomixburn.com/wp-content/uploads/2016/02/default_bg.jpg'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon/favicon.ico' }],
    script: []
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#000000' },

  /*
   ** Global CSS
   */
  css: [
    '~/client/style/page.transitions.scss',
    '~/client/style/global_styles.scss',
    '~/client/style/font_definitions.scss'
  ],

  styleResources: {
    scss: ['~/client/style/global_variables.scss']
  },

  /*
   ** Plugins to load before mounting the App
   */
  // plugins: ['~/submodules/plugins/i18n.js'],
  plugins: [
    {
      src: '~/plugins/main.js',
      ssr: true
    },
    {
      src: '~/plugins/axios.js',
      ssr: false
    },
    {
      src: '~/plugins/mixin.env.js',
      ssr: true
    },
    {
      src: '~/plugins/global.components.js',
      ssr: true
    }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    'nuxt-webfontloader',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-xxxxxxxxxxx-8',
        checkDuplicatedScript: true
      }
    ],
    [
      'nuxt-env',
      {
        keys: [
          {
            key: 'STUDIOS_DEFS',
            default: [
              {
                color: `#AF2A46`,
                logo: `ATOMIX-logo`,
                baseline: `ZURICH`,
                link: 'zurich.atomixburn.com',
                footer: {
                  name: 'Atomix Fitness',
                  address_line_1: 'Freischützgasse 10',
                  address_line_2: 'CH – 8004 Zürich'
                }
              },
              {
                color: `#3871A3`,
                logo: `ATOMIX-logo`,
                baseline: `BERLIN`,
                link: 'berlin.atomixburn.com',
                footer: {
                  name: 'Atomix Berlin GmbH',
                  address_line_1: 'Charlottenstrasse 18',
                  address_line_2: '10117 Berlin'
                }
              }
            ]
          }
        ]
      }
    ]
  ],
  /*
   ** Axios module configuration
   */
  axios: {
    credentials: true,
    proxyHeaders: false
  },

  webfontloader: {
    google: {
      families: ['Muli:400,600']
    }
  },

  // serverMiddleware: ['~api/server.api'],

  build: {
    publicPath: '/atomix/atomix-entry-page/',
    extend(config, { isDev, isClient, loaders: { vue } }) {
      if (isDev || isClient) {
        vue.transformAssetUrls.img = ['data-src', 'src']
        vue.transformAssetUrls.source = ['data-srcset', 'srcset']
      }
    },
    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        // Disable a plugin by passing false as value
        'postcss-url': false,
        'postcss-nested': {},
        'postcss-responsive-type': {},
        'postcss-hexrgba': {}
      },
      preset: {
        // Change the postcss-preset-env settings
        autoprefixer: {}
      }
    }
  },
  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      }
    }
  },
  workbox: {
    cachingExtensions: '@/plugins/workbox-range-request.js'
  },
  icon: {
    iconSrc: '/favicon/'
    // Icon options
  },
  manifest: {
    name: 'ATOMIX',
    short_name: 'ATOMIX',
    display: 'browser',
    background_color: '#000000',
    crossorigin: 'use-credentials',
    description:
      'Your workout should fit your lifestyle. That is why at ATOMIX we offer a unique studio ambiance with top-class service and personal assistance / care / attendance.',
    icons: [
      {
        src: '/favicon/android-icon-36x36.png',
        sizes: '36x36',
        type: 'image/png',
        density: '0.75'
      },
      {
        src: '/favicon/android-icon-48x48.png',
        sizes: '48x48',
        type: 'image/png',
        density: '1.0'
      },
      {
        src: '/favicon/android-icon-72x72.png',
        sizes: '72x72',
        type: 'image/png',
        density: '1.5'
      },
      {
        src: '/favicon/android-icon-96x96.png',
        sizes: '96x96',
        type: 'image/png',
        density: '2.0'
      },
      {
        src: '/favicon/android-icon-144x144.png',
        sizes: '144x144',
        type: 'image/png',
        density: '3.0'
      },
      {
        src: '/favicon/android-icon-192x192.png',
        sizes: '192x192',
        type: 'image/png',
        density: '4.0'
      },
      {
        src: '/favicon/android-icon-512x512.png',
        sizes: '512x512',
        type: 'image/png',
        density: '4.0'
      }
    ]
  }
}
