import axios from 'axios'

export const actions = {
  GET_PRODUCT(state, data) {
    const formData = new FormData()
    formData.append(
      'access_data',
      JSON.stringify({
        settings: { project_id: 38743, mode: "sandbox" },
        purchase: {
          pin_codes: { codes: [{ digital_content: data }] }
        }
      })
    )

    //  formData.append(
    //   'access_data',
    //   JSON.stringify({
    //     settings: { project_id: 38743 },
    //     purchase: {
    //       pin_codes: { codes: [{ digital_content: data }] }
    //     }
    //   })
    // )

    return axios.post(
      'https://sandbox-secure.xsolla.com/paystation2/api/pay2play/init',
      // 'https://secure.xsolla.com/paystation2/api/pay2play/init',
      formData,
      {
        headers: {
          'content-type': 'application/x-www-form-urlencoded'
        }
      }
    )
  },
  TEST_WEBP_SUPPORT(state, data) {
    return new Promise(function(resolve) {
      const img = new Image()
      img.onload = function() {
        const result = img.width > 0 && img.height > 0
        return resolve(result)
      }
      img.onerror = function() {
        return resolve(false)
      }
      img.src =
        'data:image/webp;base64,UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA=='
    }).catch(function() {
      return false
    })
  }
}

export default actions
