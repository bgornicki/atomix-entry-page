export const state = () => ({
  locales: ['en'],
  locale: 'en',
  ENV_VARS: process.ENV_VARS,
  trailerVisible: false,
  blackTrailerVisible: false,
  imageFormat: 'png',
  loadVideo: false
})

export default state
