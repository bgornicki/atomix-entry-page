import Vuex from 'vuex'
import State from './state'
import Mutations from './mutations'
import Actions from './actions'

const createStore = () => {
  return new Vuex.Store({
    state: State,
    mutations: Mutations,
    actions: Actions
  })
}

export default createStore
