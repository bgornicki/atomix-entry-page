export const mutations = {
  TOGGLE_TRAILER(state) {
    state.trailerVisible = !state.trailerVisible
  },
  TOGGLE_BLACK_TRAILER(state) {
    state.blackTrailerVisible = !state.blackTrailerVisible
  },
  SET_GLOBAL_IMAGE_FORMAT(state, format) {
    state.imageFormat = format
  },
  LOAD_VIDEO(state) {
    state.loadVideo = true
  }
}

export default mutations
