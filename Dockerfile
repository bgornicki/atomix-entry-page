FROM node:12-alpine as build

COPY . .

RUN npm run build

CMD npm start
